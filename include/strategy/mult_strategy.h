#ifndef MULT_STRATEGY_H_
#define MULT_STRATEGY_H_
#include "IStrategy.h"
#include <iostream>

namespace strategy
{
    class MultStrategy : public IStrategy
    {
    public:
        MultStrategy() : result(0) {}
        virtual ~MultStrategy()
        {
            std::cerr << "Goodbye, I was the MultStrategy" << std::endl;
        }
        void algorithm(const std::tuple<double, double> value) override
        {
            result = std::get<0>(value) * std::get<1>(value);
        }
        const double getResult() const override
        {
            return result;
        }

    private:
        double result;
    };
    std::ostream &operator<<(std::ostream &os, MultStrategy const &multStrategy)
    {
        os << "MultStrategy: result -> " << multStrategy.getResult() << std::endl;
        return os;
    }
} // namespace strategy

#endif /* MULT_STRATEGY_H_ */