#ifndef SUM_STRATEGY_H_
#define SUM_STRATEGY_H_
#include "IStrategy.h"
#include <iostream>

namespace strategy
{
    class SumStrategy : public IStrategy
    {
    public:
        SumStrategy() : result(0) {}
        virtual ~SumStrategy()
        {
            std::cerr << "Goodbye, I was the SumStrategy" << std::endl;
        }
        void algorithm(const std::tuple<double, double> value) override
        {
            result = std::get<0>(value) + std::get<1>(value);
        }
        const double getResult() const override
        {
            return result;
        }

    private:
        double result;
    };
    std::ostream &operator<<(std::ostream &os, SumStrategy const &sumstrategy)
    {
        os << "SumStrategy: result -> " << sumstrategy.getResult() << std::endl;
        return os;
    }

} // namespace strategy

#endif /* SUM_STRATEGY_H_ */