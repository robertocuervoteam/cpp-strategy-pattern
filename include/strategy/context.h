#ifndef CONTEXT_H_
#define CONTEXT_H_
#include "IStrategy.h"
#include "strategy/sum_strategy.h"
#include "strategy/div_strategy.h"
#include "strategy/mult_strategy.h"
#include <vector>
#include <memory>
#include <tuple>

namespace strategy
{

    class Context
    {

    public:
        Context();
        ~Context();
        void operation(const std::tuple<double, double> value);
        const std::vector<double> getResult() const;

    private:
        std::vector<std::shared_ptr<IStrategy>> strategies;
        std::vector<double> results;
    };

} /* namespace strategy */

#endif /* CONTEXT_H_ */