#ifndef ISTRATEGY_H_
#define ISTRATEGY_H_
#include <tuple>
namespace strategy
{
    class IStrategy
    {
    public:
        virtual ~IStrategy(){};
        virtual void algorithm(const std::tuple<double, double>) = 0;
        virtual const double getResult() const = 0;
    };

} // namespace strategy

#endif /* ISTRATEGY_H_ */