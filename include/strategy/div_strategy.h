#ifndef DIV_STRATEGY_H_
#define DIV_STRATEGY_H_
#include "IStrategy.h"
#include <iostream>

namespace strategy
{
    class DivStrategy : public IStrategy
    {
    public:
        DivStrategy() : result(0.0) {}
        virtual ~DivStrategy()
        {
            std::cerr << "Goodbye, I was the DivStrategy" << std::endl;
        }
        void algorithm(const std::tuple<double, double> value) override
        {
            if (std::get<1>(value) == 0.0)
            {
                throw std::overflow_error("Divide by zero exception");
            }
            else
            {
                result = std::get<0>(value) / std::get<1>(value);
            }
        }
        const double getResult() const override
        {
            return result;
        }

    private:
        double result;
    };
    std::ostream &operator<<(std::ostream &os, DivStrategy const &divStrategy)
    {
        os << "DivStrategy: result -> " << divStrategy.getResult() << std::endl;
        return os;
    }
} // namespace strategy

#endif /* DIV_STRATEGY_H_ */