// template <typename T, typename S, typename F>
// class Strategy
// {
// public:

//     T algorithm(T t, S s)
//     {
//         F f{};
//         return f(t, s);
//     }
// };
namespace strategy
{
    template <typename T>
    using OPERATION = T (*)(T);

    template <typename T, typename S, typename OPERATION>
    inline T algorithm(T a, S b, OPERATION operation) { return operation(a, b); }

    template <typename T>
    inline T (*function_pointer)(T a, T b);

    template <typename T, typename S, typename OPERATION>
    class Strategytest
    {
    public:
        T algorithm(T a, S b, OPERATION operation) { return operation(a, b); }
    };

} // namespace strategy