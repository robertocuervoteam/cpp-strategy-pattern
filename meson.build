project(
    'cpp-strategy-pattern', # project name
    'cpp', # C++ project, e.g: for C project
    version : '1.0.0',
    license : 'MIT',
    default_options : ['cpp_std=c++17']) # compile for C++

# it will be referred from subdir projects
inc = include_directories('include')


libstrategy = library(
    'strategy', # library name
    'src/strategy/context.cpp', # source files to be compiled
    include_directories : inc, # previously declared include directories in root :code:`meson.build`
    install : true) # :code:`libuuid` will be part of project installation

strategy = executable(
    'strategy', # executable name
    'src/main.cpp', # source files to be compiled
    include_directories : inc, # previously declared include directories in root :code:`meson.build`
    link_with : libstrategy, # linking executable with shared previously declared shared library :code:`libuuid`
    install : true) # :code:`strategy` executable be part of project installation

testexe = executable(
    'testexe', # test executable name 
    'test/strategy_test.cpp', # tests source files to be compiled
    include_directories : inc,  # declared include directories in root :code:`meson.build`
    link_with : libstrategy) # link test executable with previously declared shared library :code:`libuuid`

# test execution 
test('Strategies tests', testexe)

# we can specify other test execution passing arguments or environment variables
# test('Strategy test with args and env', testexe, args : ['arg1', 'arg2'], env : ['FOO=bar'])
