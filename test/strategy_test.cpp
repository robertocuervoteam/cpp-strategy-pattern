#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN

#include "doctest.h"
#include "strategy/strategy.h"
#include "strategy/sum_strategy.h"
#include "strategy/div_strategy.h"
#include "strategy/mult_strategy.h"
#include "strategy/context.h"
#include <functional>
#include <iostream>
template <typename T>
inline T fadd(T a, T b) { return a + b; }

template <typename T>
inline T (*function_pointer)(T a, T b);

TEST_SUITE("Strategy pattern test suite" * doctest::description("Strategy pattern unit tests"))
{
  TEST_CASE("Test Add int with function pointer")
  {
    function_pointer<int> = fadd<int>;

    int result = function_pointer<int>(10, 10);
    CHECK(result == 20);
  }

  TEST_CASE("Test Add with function int")
  {
    INFO("current iteration of loop:");
    CHECK(strategy::algorithm(2, 3, fadd<int>) == 5);
  }

  TEST_CASE("Test Add with function double")
  {
    INFO("current iteration of loop:");
    CHECK(strategy::algorithm(2.0, 3.0, fadd<double>) == 5.0);
  }

  TEST_CASE("Test add with lambda")
  {
    CHECK(strategy::algorithm(2, 2, [=](int a, int b) { return a + b; }) == 4);
  }

  TEST_CASE("Test substraction with lambda")
  {
    CHECK(strategy::algorithm(2, 2, [=](int a, int b) { return a - b; }) == 0);
  }

  TEST_CASE("Test division with lambda")
  {
    CHECK(strategy::algorithm(2, 2, [=](int a, int b) { return a / b; }) == 1);
  }

  TEST_CASE("Test multiplication with lambda")
  {
    CHECK(strategy::algorithm(2, 2, [=](int a, int b) { return a * b; }) == 4);
  }

  TEST_CASE("Test multiplication with lambda")
  {
    CHECK(strategy::algorithm(2, 2, [=](int a, int b) { return a * b; }) == 4);
  }

  TEST_CASE("Test sum  strategy")
  {
    strategy::SumStrategy sumstrategy{};
    sumstrategy.algorithm(std::make_tuple(10.0, 20.0));
    std::cout << sumstrategy;
    CHECK(sumstrategy.getResult() == doctest::Approx(30.0).epsilon(0.01));
  }
  TEST_CASE("Test division strategy")
  {
    strategy::DivStrategy divstrategy{};
    divstrategy.algorithm(std::make_tuple(10.0, 20.0));
    std::cout << divstrategy;
    CHECK(divstrategy.getResult() == doctest::Approx(0.5).epsilon(0.01));
  }
  TEST_CASE("Test division strategy with exception")
  {
    strategy::DivStrategy divstrategy{};
    CHECK_THROWS_AS(divstrategy.algorithm(std::make_tuple(10.0, 0.0)), std::overflow_error);
  }

  TEST_CASE("Test multplication strategy")
  {
    strategy::MultStrategy multstrategy{};
    multstrategy.algorithm(std::make_tuple(10.0, 2.0));
    std::cout << multstrategy;
    CHECK(multstrategy.getResult() == doctest::Approx(20.0).epsilon(0.01));
  }
  TEST_CASE("Test context")
  {
    auto value = std::make_tuple(10.0, 2.0);
    const std::vector<double> expected{5.0, 12.0, 20.0};
    strategy::Context context{};
    context.operation(value);
    auto result = context.getResult();
    for (auto i = 0; i < expected.size(); i++)
    {
      CHECK(result[i] == doctest::Approx(expected[i]).epsilon(0.01));
    }
  }
}
