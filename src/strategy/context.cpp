#include "strategy/context.h"
#include <iostream>

namespace strategy
{

    Context::Context() : results()
    {
        strategies = {std::make_shared<DivStrategy>(), std::make_shared<SumStrategy>(), std::make_shared<MultStrategy>()};
    }
    Context::~Context()
    {
        std::cerr << "Goodbye, I was a context" << std::endl;
        strategies.clear();
    }
    void Context::operation(const std::tuple<double, double> value)
    {
        std::for_each(strategies.begin(), strategies.end(), [&](auto &strategy) {
            strategy->algorithm(value);
            results.push_back(strategy->getResult());
        });
    }
    const std::vector<double> Context::getResult() const {
        return results;
    }

} /* namespace strategy */
