#include "strategy/context.h"
#include <iostream>
#include <tuple>
#include <experimental/iterator>

int main()
{
    std::cout << "Welcome to the strategy pattern!!" << std::endl;
    auto value = std::make_tuple(10.0, 2.0);
    std::cout << "We are going to calculate division, sum and multiplication of 10 and 2.\nResult:" << std::endl;
    strategy::Context context{};
    context.operation(value);
    auto result = context.getResult();
    std::copy(result.begin(),
              result.end(),
              std::experimental::make_ostream_joiner(std::cout, "\n"));
}
