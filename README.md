# Strategy pattern 
Example of the strategy pattern implemented in C++17 and built with Meson Build

This project consists of:

- a library (let's name it `strategies`)
- an executable `strategy` using that `strategies`
- unit test executable for `strategies` and using doctest framework 

## How to build the example


```bash
meson build
cd build
ninja
ninja test # to run unit tests or
meson test
```

##  How to test or run the tests

```bash
# from whithin the build directory
ninja test # or 
meson test -v  # verbose output
# from within project directory
meson test -C build -v
```
When running `meson test -v` the error below can happen:
```bash
cpp_projects/cpp-strategy-pattern/build/testexe@exe/test_strategy_test.cpp.gcda: cannot merge previous run count: corrupt object tag (0x686331
```

Remove the file `build/testexe@exe/test_strategy_test.cpp.gcda`
And run again `meson test -v`

## How to get code coverage

Remove `build/` if exists. 

Run:

```bash
meson build -Db_coverage=true
cd build
meson test # You can only generate coverage reports after running the tests (running the tests is required to gather the list of functions that get called).
ninja coverage-html
```

Open the html file located `strategy-pattern/build/meson-logs/coveragereport/index.html`


## How to install


### In default system directory

```bash
ninja install
```

You can run it just with command `strategy`.


### In specific directory, e.g: ./install


```bash
DESTDIR=./install ninja install
```
Running `strategy` is a little bit more difficult in this case because the installation directory might not belong to library path (`LD_LIBRARY_PATH`). 

```bash
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:./install/usr/local/lib
./install/usr/local/bin/strategy #path to installed binary
```
